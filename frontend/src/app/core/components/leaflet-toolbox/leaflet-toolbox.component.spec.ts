import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeafletToolboxComponent } from './leaflet-toolbox.component';

describe('LeafletToolboxComponent', () => {
  let component: LeafletToolboxComponent;
  let fixture: ComponentFixture<LeafletToolboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LeafletToolboxComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LeafletToolboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
