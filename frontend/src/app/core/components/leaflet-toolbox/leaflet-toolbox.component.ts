import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { SkillcapAnimations } from '../../helper/animation.helper';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.state';
import { toggleAddingMarkerMode } from '../../../store/cctv.store';

@Component({
  selector: 'leaflet-toolbox',
  standalone: true,
  imports: [CommonModule, RouterModule],
  animations: SkillcapAnimations,
  templateUrl: './leaflet-toolbox.component.html',
  styleUrl: './leaflet-toolbox.component.scss'
})
export class LeafletToolboxComponent {
  @Output() confirmMarkerClicked = new EventEmitter();
  @Input() addingMarkerMode: boolean = false;
  fabTogglerState = 'inactive';
  isDropdownOpen: boolean = false;

  constructor(private store: Store<AppState>) {}

  toggleDropdown() {
    if (this.isDropdownOpen) {
      this.startAnimation('inactive');
    } else {
      this.store.dispatch(toggleAddingMarkerMode({ addMarkerMode: false }));
      this.startAnimation('active');
    }
    this.isDropdownOpen = !this.isDropdownOpen;
  }

  confirmMarkerEmit() {
    this.confirmMarkerClicked.emit();
  }

  @HostListener('document:click', ['$event'])
  closeMenu(event: MouseEvent) {
    if (!this.isDropdownOpen) {
      return;
    } else {
      if (this.checkIfClickedOutsideMenu(event)) {
        this.isDropdownOpen = false;
        this.startAnimation('inactive');
      }
    }
  }

  enterAddMarkerMode() {
    this.store.dispatch(toggleAddingMarkerMode({ addMarkerMode: true }));
  }

  private checkIfClickedOutsideMenu(event: MouseEvent) {
    const target = event.target as HTMLElement;
    if (target.matches('.toolboxFab')) {
      return false;
    } else {
      return true;
    }
  }

  startAnimation(state: string) {
    if (state === 'active') {
      this.fabTogglerState = 'active';
    } else {
      this.fabTogglerState = 'inactive';
    }
  }
}
