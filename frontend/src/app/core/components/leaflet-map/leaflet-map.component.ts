import { Component, EventEmitter, Inject, Input, Output, PLATFORM_ID } from '@angular/core';
import { LeafletHelper } from '../../helper/leaflet.helper';
import { CommonModule } from '@angular/common';
import { CameraInterface } from '../../../interfaces/camera.interface';

@Component({
  selector: 'leaflet-map',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './leaflet-map.component.html',
  styleUrl: './leaflet-map.component.scss'
})
export class LeafletMapComponent {
  // declare L to enable window.L later in updateCameras
  private L = null;

  map: any;
  markerToAdd: any;
  markerClusterGroup: any;
  @Input() addingMarkerMode: boolean = false;
  @Output() setMarkerPositionEmit = new EventEmitter();
  @Output() mapReadyEmit = new EventEmitter();

  constructor(
    private leafletHelper: LeafletHelper,
    @Inject(PLATFORM_ID) private _platformId: Object
  ) {}

  async ngOnInit() {
    if (this._platformId === 'browser') {
      this.L = await import('leaflet');
      await import('leaflet.markercluster');
      this.leafletHelper.loadLeaflet().then((lib) => {
        this.initMap(lib);
        this.map.on('click', this.mapClicked.bind(this));
      })
    }
  }

  initMap(lib: any): void {
    this.map = lib.map('map', {
      layers: [
        lib.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 21,
          maxNativeZoom: 19,
          minZoom: 3,
          attribution: '...',
          noWrap: true,
        }),
      ],
      attributionControl: false,
      zoom: 15,
      // center on berlin
      center: lib.latLng(52.52437, 13.41053),
    });
    this.mapReadyEmit.emit()
  }

  // call this from parent to update markers
  updateCameraMarkers(cameras: CameraInterface[]): void {
    this.leafletHelper.loadLeaflet().then(lib => {
      if (!this.map) return;
      this.resetMap();
      this.markerClusterGroup = window.L.markerClusterGroup();
      cameras.forEach(camera => {
        let marker = this.leafletHelper.createCameraMarker(lib, camera.lat, camera.lng);
        marker.addTo(this.markerClusterGroup);
      });
      this.markerClusterGroup.addTo(this.map);
    });
  }

  mapClicked(event: any) {
    if (!this.addingMarkerMode) {
      return;
    }
    this.resetToAddCameraMarkerLayer()
    this.leafletHelper.loadLeaflet().then(leafletLib => {
      this.markerToAdd = this.leafletHelper.createCameraMarker(leafletLib, event.latlng.lat, event.latlng.lng);
      this.setMarkerPositionEmit.emit(this.markerToAdd)
      this.map.addLayer(this.markerToAdd);
    })
  }

  resetMap() {
    this.resetToAddCameraMarkerLayer()
    if (this.markerClusterGroup) {
      this.markerClusterGroup.clearLayers();
    }
  }

  resetToAddCameraMarkerLayer() {
    if (this.markerToAdd) {
      this.map.removeLayer(this.markerToAdd);
      this.markerToAdd = null;
    }
  }
}
