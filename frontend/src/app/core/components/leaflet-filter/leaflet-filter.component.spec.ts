import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeafletFilterComponent } from './leaflet-filter.component';

describe('LeafletFilterComponent', () => {
  let component: LeafletFilterComponent;
  let fixture: ComponentFixture<LeafletFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LeafletFilterComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LeafletFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
