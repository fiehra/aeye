import { CommonModule } from '@angular/common';
import { Component, HostListener } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SkillcapAnimations } from '../../helper/animation.helper';
import { toggleAddingMarkerMode } from '../../../store/cctv.store';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.state';

@Component({
	selector: 'menu',
	standalone: true,
	imports: [CommonModule, RouterModule],
	templateUrl: './menu.component.html',
	animations: SkillcapAnimations,
	styleUrl: './menu.component.scss'
})
export class MenuComponent {
	isDropdownOpen: boolean = false;
	fabTogglerState = 'inactive';

	constructor(private store: Store<AppState>) {}

	toggleDropdown() {
		if (this.isDropdownOpen) {
			this.startAnimation('inactive');
		} else {
			this.startAnimation('active');
      this.store.dispatch(toggleAddingMarkerMode({ addMarkerMode: false }));
		}
		this.isDropdownOpen = !this.isDropdownOpen;
	}

	@HostListener('document:click', ['$event'])
	closeMenu(event: MouseEvent) {
    if (!this.isDropdownOpen) {
      return;
    } else {
      if (this.checkIfClickedOutsideMenu(event)) {
        this.isDropdownOpen = false;
        this.startAnimation('inactive');
      }
    }
	}

	private checkIfClickedOutsideMenu(event: MouseEvent) {
		const target = event.target as HTMLElement;
		if (target.matches('.menuFab')) {
			return false;
		} else {
			return true;
		}
	}

	startAnimation(state: string) {
		if (state === 'active') {
			this.fabTogglerState = 'active';
		} else {
			this.fabTogglerState = 'inactive';
		}
	}
}
