import { Injectable, PLATFORM_ID, Inject } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LeafletHelper {
  public L = null;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    if (this.platformId === 'browser') {
      this.L = import('leaflet');
    }
  }

  async loadLeaflet() {
    this.L = await this.L;
    return this.L;
  }

  createPoliceMarker(lib: any, lat: number, lng: number) {
    return lib.marker([lat, lng], {
      icon: lib.icon({
        iconAnchor: [12, 12],
        iconUrl: '/images/policeMarker.png',
        iconRetinaUrl: '/images/policeMarker.png',
      }),
    });
  }

  createCameraMarker(lib: any, lat: number, lng: number) {
    return lib.marker([lat, lng], {
      icon: lib.icon({
        iconAnchor: [12, 12],
        iconUrl: '/images/cameraMarker.png',
        iconRetinaUrl: '/images/cameraMarker.png',
      }),
    });
  }
}
