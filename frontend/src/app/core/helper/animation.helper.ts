
import { animate, state, style, transition, trigger } from '@angular/animations';

export const SkillcapAnimations = [
  trigger('mainMenuToggler', [
    state(
      'inactive',
      style({
        transform: 'rotate(0deg)',
      }),
    ),
    state(
      'active',
      style({
        transform: 'rotate(135deg)',
      }),
    ),
    transition('* <=> *', animate('250ms cubic-bezier(0.2, 0.0, 0.2, 1)')),
  ]),

];
