
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CameraCreateDTO, CameraInterface } from '../../interfaces/camera.interface';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CctvService {
  constructor(private http: HttpClient) {}


  addCamera(camera: CameraCreateDTO) {
    return this.http.post(environment.backendUrl + '/cctv/create', camera);
  }

  deleteCamera(camera: CameraInterface) {
    return this.http.delete<{
      message: string;
      cameras: CameraInterface[];
    }>(environment.backendUrl + '/cctv/delete/' + camera._id);
  }

  getCameraById(_id: string) {
    return this.http.get<{
      message: string;
      camera: CameraInterface;
    }>(environment.backendUrl + '/cctv/getOne/' + _id);
  }

  getCameras() {
    return this.http.get<{
      message: string;
      cameras: CameraInterface[];
    }>(environment.backendUrl + '/cctv/getAll');
  }
}
