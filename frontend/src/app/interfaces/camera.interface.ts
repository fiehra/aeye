export interface CameraInterface {
  _id: string;
  model: string;
  ownership: string;
	lat: number;
	lng: number;
  verified: boolean;
}

export interface CameraCreateDTO {
  model: string;
  ownership: string;
  lat: number;
  lng: number;
  verified: boolean;
}
