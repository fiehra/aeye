import { Routes } from '@angular/router';
import { HomePageComponent } from './features/infrastructure/home-page/home-page.component';
import { NotFoundPageComponent } from './features/infrastructure/not-found-page/not-found-page.component';

export const routes: Routes = [
	{ path: '', component: HomePageComponent },
	{ path: 'map', loadComponent:()=> import('./features/cctv-map/cctv-map.component').then((m) => m.CctvMapComponent)},
	{ path: 'privacy', loadComponent:()=> import('./features/infrastructure/privacy-page/privacy-page.component').then((m) => m.PrivacyPageComponent)},
	{ path: 'imprint', loadComponent:()=> import('./features/infrastructure/imprint-page/imprint-page.component').then((m) => m.ImprintPageComponent)},
	{ path: '**', component: NotFoundPageComponent },
];
