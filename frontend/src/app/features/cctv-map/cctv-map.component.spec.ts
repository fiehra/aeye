import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CctvMapComponent } from './cctv-map.component';

describe('CctvMapComponent', () => {
  let component: CctvMapComponent;
  let fixture: ComponentFixture<CctvMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CctvMapComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CctvMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
