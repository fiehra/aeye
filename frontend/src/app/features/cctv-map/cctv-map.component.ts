import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { LeafletMapComponent } from '../../core/components/leaflet-map/leaflet-map.component';
import { MenuComponent } from '../../core/components/menu/menu.component';
import { LeafletToolboxComponent } from '../../core/components/leaflet-toolbox/leaflet-toolbox.component';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { CameraCreateDTO, CameraInterface } from '../../interfaces/camera.interface';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.state';
import { addCamera, loadCameras, selectAddingMarkerMode, selectCameras, toggleAddingMarkerMode } from '../../store/cctv.store';

@Component({
  selector: 'cctv-map',
  standalone: true,
  imports: [LeafletMapComponent, MenuComponent, LeafletToolboxComponent, CommonModule],
  providers: [],
  templateUrl: './cctv-map.component.html',
  styleUrl: './cctv-map.component.scss'
})
export class CctvMapComponent implements OnInit, OnDestroy {
  addingMarkerMode: boolean = false;
  addingMarkerMode$: Observable<boolean> = this.store.select(selectAddingMarkerMode);
  addMarkerModeSub: Subscription;
  cameras: CameraInterface[] = [];
  cameras$: Observable<CameraInterface[]> = this.store.select(selectCameras);
  camerasSub: Subscription;

  @ViewChild(LeafletMapComponent) leafletMapComponent: LeafletMapComponent;

  cameraForm = new FormGroup({
    model: new FormControl(''),
    ownership: new FormControl(''),
    lat: new FormControl(null),
    lng: new FormControl(null),
    verified: new FormControl(false),
  })

  constructor(
    private store: Store<AppState>,
  ) {
    this.store.dispatch(loadCameras());
  }

  ngOnInit() {
    this.addMarkerModeSub = this.addingMarkerMode$.subscribe(addMarkerMode => {
      this.addingMarkerMode = addMarkerMode;
    })
    this.camerasSub = this.cameras$.subscribe(cameras => {
      this.cameras = cameras;
      if (this.leafletMapComponent) {
        this.leafletMapComponent.updateCameraMarkers(cameras);
      }
    });
  }

  updateMap() {
    if (this.cameras) {
      this.leafletMapComponent.updateCameraMarkers(this.cameras);
    }
  }

  placedMarkerPosition(marker:L.Marker) {
    this.cameraForm.get('lat')?.setValue(marker.getLatLng().lat);
    this.cameraForm.get('lng')?.setValue(marker.getLatLng().lng);
  }

  confirmMarker() {
    this.store.dispatch(toggleAddingMarkerMode({addMarkerMode: false}));
    if (this.cameraForm.get('lat')?.value && this.cameraForm.get('lng')?.value) {
      let cameraDto: CameraCreateDTO = {
        model: 'model',
        ownership: 'owned',
        lat: this.cameraForm.get('lat')?.value,
        lng: this.cameraForm.get('lng')?.value,
        verified: false,
      }
      this.store.dispatch(addCamera({camera: cameraDto}))
      this.cameraForm.get('lat')?.setValue(null);
      this.cameraForm.get('lng')?.setValue(null);
    } else {
      console.log('no lat/lng');
    }
  }

  ngOnDestroy() {
    this.addMarkerModeSub.unsubscribe();
    this.camerasSub.unsubscribe();
  }
}

