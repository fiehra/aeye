import { Component } from '@angular/core';
import { FooterComponent } from '../../../core/components/footer/footer.component';
import { MenuComponent } from '../../../core/components/menu/menu.component';
import { SsrHelper } from '../../../core/helper/ssr.helper';

@Component({
  selector: 'privacy-page',
  standalone: true,
  imports: [MenuComponent, FooterComponent],
  templateUrl: './privacy-page.component.html',
  styleUrl: './privacy-page.component.scss'
})
export class PrivacyPageComponent {

  constructor(private ssrHelper: SsrHelper) {

  }

  ngOnInit() {
    this.ssrHelper.scrollTop();
  }
}
