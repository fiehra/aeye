import { Component } from '@angular/core';
import { FooterComponent } from '../../../core/components/footer/footer.component';
import { MenuComponent } from '../../../core/components/menu/menu.component';
import { SsrHelper } from '../../../core/helper/ssr.helper';

@Component({
  selector: 'imprint-page',
  standalone: true,
  imports: [FooterComponent, MenuComponent],
  templateUrl: './imprint-page.component.html',
  styleUrl: './imprint-page.component.scss'
})
export class ImprintPageComponent {

  constructor(private ssrHelper: SsrHelper) {

  }

  ngOnInit() {
    this.ssrHelper.scrollTop();
  }
}
