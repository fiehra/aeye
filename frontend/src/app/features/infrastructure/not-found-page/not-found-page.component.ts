import { Component } from '@angular/core';
import { MenuComponent } from '../../../core/components/menu/menu.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'not-found-page',
  standalone: true,
  imports: [MenuComponent, RouterModule],
  templateUrl: './not-found-page.component.html',
  styleUrl: './not-found-page.component.scss'
})
export class NotFoundPageComponent {

}
