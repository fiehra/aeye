import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LandingComponent } from '../../../core/components/landing/landing.component';
import { FooterComponent } from '../../../core/components/footer/footer.component';
import { MenuComponent } from '../../../core/components/menu/menu.component';
import { InfoSectionComponent } from '../../../core/components/info-section/info-section.component';
import { SsrHelper } from '../../../core/helper/ssr.helper';

@Component({
  selector: 'app-home-page',
  standalone: true,
  imports: [FooterComponent,MenuComponent, RouterModule, LandingComponent, InfoSectionComponent],
  templateUrl: './home-page.component.html',
  styleUrl: './home-page.component.scss'
})
export class HomePageComponent {

  constructor(private ssrHelper: SsrHelper) { }

  ngOnInit() {
    this.ssrHelper.scrollTop();
  }

}
