import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveillanceMapComponent } from './surveillance-map.component';

describe('SurveillanceMapComponent', () => {
  let component: SurveillanceMapComponent;
  let fixture: ComponentFixture<SurveillanceMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SurveillanceMapComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SurveillanceMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
