
import { ActionReducerMap } from '@ngrx/store';
import { CctvState, cctvReducer } from './cctv.store';

export interface AppState {
  cctvState: CctvState;
}

export const appReducer: ActionReducerMap<AppState> = {
  cctvState: cctvReducer,
};
