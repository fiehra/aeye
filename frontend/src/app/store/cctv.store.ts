
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { createAction, createReducer, createSelector, on, props } from '@ngrx/store';
import { switchMap } from 'rxjs/operators';
import { AppState } from './app.state';
import { CameraCreateDTO, CameraInterface } from '../interfaces/camera.interface';
import { CctvService } from '../core/services/cctv.service';

export const loadCameras = createAction(
  '[CCTV] load all cameras',
);
export const loadCamerasSuccess = createAction(
  '[CCTV] load all cameras success',
  props<{ cameras: CameraInterface[] }>(),
);
export const addCamera = createAction(
  '[CCTV] add camera',
  props<{ camera: CameraCreateDTO }>(),
);
export const addCameraSuccess = createAction(
  '[CCTV] add camera success',
  props<{ cameras: CameraInterface[] }>(),
);
export const toggleAddingMarkerMode = createAction(
  '[CCTV] toggle adding marker mode',
  props<{ addMarkerMode: boolean }>(),
);

export interface CctvState {
  cameras: CameraInterface[];
  mapLoading: boolean;
  addingMarkerMode: boolean;
}
export const intialState: CctvState = {
  cameras: [],
  mapLoading: false,
  addingMarkerMode: false,
};

export const cctvReducer = createReducer(
  intialState,
  on(loadCameras, (state) => ({
    ...state,
    mapLoading: true,
  })),
  on(loadCamerasSuccess, (state, {cameras}) => ({
    ...state,
    cameras: cameras,
    mapLoading: false,
  })),
  on(toggleAddingMarkerMode, (state, {addMarkerMode}) => ({
    ...state,
    addingMarkerMode: addMarkerMode,
  })),
);

export function cctvReducerFn(state: CctvState, action) {
  return cctvReducer(state, action);
}

export const cctvFeature = (state: AppState) => state.cctvState;

export const selectCameras = createSelector(
  cctvFeature,
  (state: CctvState) => state.cameras,
);
export const selectAddingMarkerMode = createSelector(
  cctvFeature,
  (state: CctvState) => state.addingMarkerMode,
);

@Injectable()
export class CctvEffects {
  constructor(private actions$: Actions, private cctvService: CctvService) {}

  loadCameras$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadCameras),
      switchMap(() =>
        this.cctvService.getCameras().pipe(
          switchMap((response:any) => {
            return [loadCamerasSuccess({ cameras: response.cameras })];
          }),
        ),
      ),
    ),
  );

  addCamera$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addCamera),
      switchMap((action) =>
        this.cctvService.addCamera(action.camera).pipe(
          switchMap((response:any) => {
            return [loadCamerasSuccess({ cameras: response.cameras })];
          }),
        ),
      ),
    ),
  );
}
