export const environment = {
  production: false,
  authRedirectUrl: 'http://localhost:4200/',
  backendUrl: 'http://localhost:2211/api',
  // backendUrl: 'http://192.168.178.24:5555/api',
  // backendUrl: 'http://192.168.178.88:5555/api',
};

