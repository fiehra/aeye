/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,ts}'],
	theme: {
		colors: {
			'primary': '#DC1724',
			'primaryHover': '#EC4651',
			'secondary': '#94B9AF',
			'secondaryHover': '#C1D7D1',
			'background': '#2A2828',
			'backgroundHover': '#333030',
			'font': '#E6EFEC',
		},
	},
	plugins: [],
};
