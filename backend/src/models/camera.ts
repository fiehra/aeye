import mongoose, { Schema, model } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export interface CameraInterface {
  _id: mongoose.Types.ObjectId;
  model: string;
  ownership: string;
	lat: number;
	lng: number;
  verified: boolean;
}

const cameraSchema = new Schema<CameraInterface>({
  _id: { type: mongoose.Schema.Types.ObjectId },
  model: { type: String, required: true },
  ownership: { type: String, required: true },
  lat: { type: Number, required: true },
  lng: { type: Number, required: true },
  verified: { type: Boolean, default: false },
});

cameraSchema.plugin(uniqueValidator);
export default model('Camera', cameraSchema);

