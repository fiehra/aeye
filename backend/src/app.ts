import * as dotenv from 'dotenv';
dotenv.config();
import 'reflect-metadata';

import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import helmet from 'helmet';

import { container } from 'tsyringe';
import { CctvRoutes } from './cctv/cctv.routes';

const app = express();

app.use(compression());
app.use(helmet());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  }),
);

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization',
  );
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
  next();
});
app.use('/api', container.resolve(CctvRoutes).router);

export default app;

