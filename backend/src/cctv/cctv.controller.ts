import { Request, Response } from 'express';
import { autoInjectable, container } from 'tsyringe';
import { CctvService } from './cctv.service';

@autoInjectable()
export class CctvController {
  constructor() {}

  addMarker(req: Request, res: Response) {
    container.resolve(CctvService).createCameraMarker(req, res);
  }

  removeMarker(req: Request, res: Response) {
    container.resolve(CctvService).deleteCameraMarker(req, res);
  }

  fetchAllMarker(req: Request, res: Response) {
    container.resolve(CctvService).getAllCameraMarker(req, res);
  }
}

