import mongoose from 'mongoose';
import { Request, Response } from 'express';
import { autoInjectable } from 'tsyringe';
import Camera, { CameraInterface } from '../models/camera';
import { CctvRepository } from './cctv.repository';

@autoInjectable()
export class CctvService {
  constructor(
    private cctvRepository: CctvRepository
  ) {}

  async createCameraMarker(req: Request, res: Response) {
    try {
      const camera: CameraInterface = new Camera({
        _id: new mongoose.Types.ObjectId(),
        model: req.body.model,
        ownership: req.body.ownership,
        lat: req.body.lat,
        lng: req.body.lng,
        verified: req.body.verified,
      });
      const result = await this.cctvRepository.save(camera);
      const cameras = await this.cctvRepository.findAll();
      if (result) {
        return res.status(201).json({
          cameras: cameras,
          message: 'camera created',
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'adding camera failed',
      });
    }
  }

  async deleteCameraMarker(req: Request, res: Response) {
    try {
      await this.cctvRepository.deleteOne({ _id: req.params.id })
      const cameras = await this.cctvRepository.findAll();
      return res.status(200).json({
        cameras: cameras,
        message: 'camera deleted',
      });
    } catch (error) {
      return res.status(500).json({
        error: error,
        message: 'deleting camera failed',
      });
    };
  }

  async getAllCameraMarker(req: Request, res: Response) {
    try {
      const cameras = await this.cctvRepository.findAll();
      return res.status(200).json({
        cameras: cameras,
        message: 'cameras fetched',
      });
    } catch (error) {
      return res.status(500).json({
        error: error,
        message: 'fetching cameras failed',
      });
    }
  }
}
