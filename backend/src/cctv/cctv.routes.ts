
import express from 'express';
import { autoInjectable } from 'tsyringe';
import { CctvController } from './cctv.controller';

@autoInjectable()
export class CctvRoutes {
  router = express.Router();

  constructor(private cctvController: CctvController) {
    this.configureRoutes();
  }

  configureRoutes() {
    this.router.post('/cctv/create', this.cctvController.addMarker);
    this.router.delete('/cctv/delete/:id', this.cctvController.removeMarker);
//    this.router.get('/cctv/getOne/:id', this.cctvController.getMarkerById);
    this.router.get('/cctv/getAll', this.cctvController.fetchAllMarker);
  }
}
