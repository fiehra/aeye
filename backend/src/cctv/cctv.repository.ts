
import { autoInjectable } from 'tsyringe';
import Camera, { CameraInterface } from '../models/camera';

@autoInjectable()
export class CctvRepository {

  async save(camera: CameraInterface) {
    return Camera.create(camera);
  }

  async findAll() {
    return Camera.find().sort({ name: 'desc' }).select('-__v');
  }

  async findOne(query: any) {
    return Camera.findOne(query);
  }

  async updateOne(query: any, camera: CameraInterface) {
    return Camera.updateOne(query, camera);
  }

  async deleteOne(query: any) {
    return Camera.deleteOne(query);
  }
}
