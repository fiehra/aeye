import app from './app';
import mongoose from 'mongoose';

const normalizePort = (val) => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
};

const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof port === 'string' ? 'pipe ' + port : 'port ' + port;
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
    default:
      throw error;
  }
};

const port = normalizePort(process.env.PORT);

app.on('error', onError);
app.listen(port, () => {
  console.log(`aEye is listening on port ${process.env.PORT}`);
});

const url = process.env.MONGO + '';

mongoose.set('strictQuery', false);
mongoose
  .connect(url, {})
  .then(() => {
    console.log('connected to aEye database on ' + new Date().toDateString());
  })
  .catch((error) => {
    console.log(error);
    console.log('connection failed');
  });

