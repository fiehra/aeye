import app from '../src/app';
import mongoose from 'mongoose';
import request from 'supertest';
import Camera from '../src/models/camera';

describe('cctvModule', () => {
  const url = process.env.MONGO + '';

  const cameraId = new mongoose.Types.ObjectId();
  const cameraOne = {
    _id: cameraId,
    model: 'camera',
    ownership: 'public',
    lat: 35.6895,
    lng: 139.69171,
    verified: false,
  };

  beforeAll(async () => {
    await mongoose.disconnect();
    await mongoose.connect(url, {});
    await new Camera(cameraOne).save();
  });

  beforeEach(async () => {});

  afterAll(async () => {
    await Camera.deleteMany();
    await mongoose.disconnect();
  });

  test('create camera marker success 201', async () => {
    const berlinLat = 52.52437;
    const berlinLng = 13.41053;
    const cameraCountBefore = await Camera.countDocuments();
    const response = await request(app)
      .post('/api/cctv/create')
      .send({
        model: 'cctvCamera',
        ownership: 'public',
        lat: berlinLat,
        lng: berlinLng,
        verified: false,
      })
      .expect(201);
    expect(response.body.message).toBe('camera created');
    expect(cameraCountBefore + 1).toBe(await Camera.countDocuments());
  });

  test('create camera marker failure 500', async () => {
    const response = await request(app)
      .post('/api/cctv/create')
      .send({})
      .expect(500);
    expect(response.body.message).toBe('adding camera failed');
  });

  test('delete camera marker success 200', async () => {
    const camera = await Camera.findOne();
    const response = await request(app)
      .delete('/api/cctv/delete/' + cameraId)
      .expect(200);
    expect(response.body.message).toBe('camera deleted');
    expect(await Camera.countDocuments()).toBe(1);
  });

  test('delete camera marker failure 500', async () => {
    const response = await request(app)
      .delete('/api/cctv/delete/invalidid')
      .expect(500);
    expect(response.body.message).toBe('deleting camera failed');
  });

  test('get all camera markers success 200', async () => {
    const response = await request(app)
      .get('/api/cctv/getAll')
      .expect(200);
    expect(response.body.message).toBe('cameras fetched');
    expect(await Camera.countDocuments()).toBe(1);
  });

  test('get all camera markers failure 500', async () => {
    mongoose.disconnect(); // Disconnect to simulate a database error
    const response = await request(app)
      .get('/api/cctv/getAll')
      .expect(500);
    expect(response.body.message).toBe('fetching cameras failed');
    await mongoose.connect(url, {}); // Reconnect for subsequent tests
  });
});

